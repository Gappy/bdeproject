/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.main.bdeproject.service;

import com.main.bdeproject.view.ConsoleOutput;
import com.owlike.genson.Genson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author Windows-Work
 */
public class ServiceEvenement {
     
   
    public List<Object> getLstEvenement(){

        List<Object> lst = getRequest();        
        for(Object obj : lst){
            ConsoleOutput.addOutPut(obj.toString());
        }
        
        return lst;
    }
    
      public List<Object> getLstEvenement(int id){

        List<Object> lst = getRequest(id);        
        for(Object obj : lst){
            ConsoleOutput.addOutPut(obj.toString());
        }
        
        return lst;
    }
    
    public List<Object> getRequest(int id)
    {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri("http://127.0.0.1:9001").build());
         
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("id", String.valueOf(id));
        
        String json;
        json = service.path("bdeproject").path("evenement").queryParams(queryParams).accept(MediaType.APPLICATION_JSON).get(String.class);
   
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        
        List<Object> lst = null;

        try{
          lst = genson.deserialize(json, List.class);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
  
        return lst;
    }
      
    
    public List<Object> getRequest()
    {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri("http://127.0.0.1:9001").build());
        
        String json;
        json = service.path("bdeproject").path("evenement").accept(MediaType.APPLICATION_JSON).get(String.class);
        
        // Genson genson = new Genson();
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        
        List<Object> lst = null;
        
       // List<Eleve> lst = null;
        
        try{
          lst = genson.deserialize(json, List.class);
            //lst = genson.deserialize(json, new GenericType<List<Eleve>>(){});
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
      
        //List<Eleve> listEleve = (List<Object>)(List<?>)lst;
        
     //   System.out.println(lst.size());
        
        return lst;
    }
}
