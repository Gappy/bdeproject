/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.main.bdeproject.service;

import com.json.parsers.JsonParserFactory;
import com.main.bdeproject.bean.Utilisateur;
import com.main.bdeproject.view.ConsoleOutput;
import com.owlike.genson.Genson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.JTable;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.json.parsers.JSONParser;

/**
 *
 * @author Windows-Work
 */
public class ServiceUtilistateur {
    public List<Object> getLstUtilistateur(){
         List<Object> result = getRequest();
         
         List<Utilisateur> listUtilisateur = new ArrayList<>();
         
         
         for(Object obj : result){
              ConsoleOutput.addOutPut(obj.toString());
             //System.out.println(obj.toString());
             //listUtilisateur.add(new Utilisateur(null, null));
         }
       
        return result;
    }
    
     public List<Object> getRequest()
    {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri("http://127.0.0.1:9001").build());
        
        String json;
        
       json = service.path("bdeproject").path("utilisateur").accept(MediaType.APPLICATION_JSON).get(String.class);
        
        // getting XML data
        System.out.println(json);
        // getting JSON data
        
        // Genson genson = new Genson();
        //Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        Genson genson = new Genson.Builder().create();
        
        List<Object> lst = null;
        Map<Object,Object> map = null;

        try{
      
          lst = genson.deserialize(json, List.class);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        /* JsonParserFactory factory=JsonParserFactory.getInstance();
         JSONParser parser = factory.newJsonParser();
         Map jsonData = parser.parseJson(json);
      
         Map rootJson = (Map)jsonData.get("root");
         List al = (List)rootJson.get("@class");
         
         ConsoleOutput.addOutPut(jsonData.toString());*/
        // String value=(String)jsonData.get("name");
        
        return lst;
    }
}
