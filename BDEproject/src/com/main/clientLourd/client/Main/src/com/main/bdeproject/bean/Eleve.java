package com.main.bdeproject.bean;

public class Eleve {

	private static int compteur = 1;
	private int idEleve;
	private String nomEleve;
	private String prenomEleve;
	private String nomClasse;

	public Eleve() {
		this.idEleve = compteur;
		compteur++;
	}

	public Eleve(String nom, String prenom, String nomClasse) {
		this();
		this.nomEleve = nom;
		this.prenomEleve = prenom;
		this.nomClasse = nomClasse;
	}

	public int getIdEleve() {
		return idEleve;
	}

	public void setIdEleve(int idEleve) {
		this.idEleve = idEleve;
	}

	public String getNomEleve() {
		return nomEleve;
	}

	public void setNomEleve(String nomEleve) {
		this.nomEleve = nomEleve;
	}

	public String getPrenomEleve() {
		return prenomEleve;
	}

	public void setPrenomEleve(String prenomEleve) {
		this.prenomEleve = prenomEleve;
	}

	public String getNomClasse() {
		return nomClasse;
	}

	public void setNomClasse(String nomClasse) {
		this.nomClasse = nomClasse;
	}
	
	public String toString(){
		return idEleve+" "+nomEleve+" "+prenomEleve+" "+nomClasse;
	}

}
