package com.main.bdeproject.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.main.bdeproject.bean.Utilisateur;
import com.main.bdeproject.dao.ConnexionBdd;
import com.owlike.genson.Genson;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class ServiceUtilisateur extends ServerResource {

	private ConnexionBdd connexion;
	
	public ServiceUtilisateur(){
		
		this.connexion = ConnexionBdd.getInstance();
	}
	
	// @post
	public Utilisateur creerUtilisateur(String login, String password) {
		Utilisateur utilis = new Utilisateur(login, password);
		connexion.update("INSERT INTO Utilisateur (nom_Utilisateur,mdp)  VALUES('" + utilis.getLoginUtilisateur() + "','"+ utilis.getPassword() + "')");
		return utilis;
	}
	
	// @put
	public Utilisateur majUtilisateur(int id, String login, String password) {
		connexion.update("UPDATE Utilisateur SET nom_utilisateur='"+login+"', mdp='"+password+"'WHERE id_utilisateur="+ id);
		ResultSet rs = connexion.query("SELECT * FROM Utilisateur where id_utilisateur="+ id);
		Utilisateur myUser=new Utilisateur();
		int idUser;
		String loginUser;
		String passwordUser;
		try {
			while (rs.next()) {
				
				idUser = rs.getInt(1);
				loginUser = rs.getString(2);
				passwordUser = rs.getString(3);
		
				myUser.setIdUtilisateur(idUser);
				myUser.setLoginUtilisateur(loginUser);
				myUser.setPassword(passwordUser);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return myUser;
		}

   /*
        http://192.168.0.1:9001/bdeproject/utilisateur
        http://192.168.0.1:9001/bdeproject/utilisateur?id=5
    */
    @Get
    public String getUtilitateurWebService() {
        int id;

        try {
            id = Integer.valueOf(getQuery().getValues("id"));
        }
        catch (Exception ex) {
            id = -1;
        }

        List<Utilisateur> lst;

        if(id > 0) {
            lst = getUtilisateur(id);
        }else {
            lst = getUtilisateur();
        }

        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();

        String json;
        try {
            json = genson.serialize(lst);
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        return json;
    }

	public List<Utilisateur> getUtilisateur() {
		return getUtilisateur(-1);
	}
	
	public List<Utilisateur> getUtilisateur(int id) {
		String myquery;
		if(id<0){
		myquery="select * from Utilisateur";
		}else{
		myquery="select * from Utilisateur where id_utilisateur="+id;
		}
		ResultSet rs = connexion.query(myquery);
		List<Utilisateur> listUtilisateur=new ArrayList<Utilisateur>();
		try {
			while (rs.next()) {
                String idUtilisateur = rs.getString(1);
				String login = rs.getString(2);
				String password = rs.getString(3);
				Utilisateur myUser = new Utilisateur(Integer.valueOf(idUtilisateur),login, password);
				listUtilisateur.add(myUser);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listUtilisateur;
	}
}
