package com.main.bdeproject.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class ConnexionBdd {

    private String DBPath = "Chemin aux base de donn�e SQLite";
    private Connection connection = null;
    private Statement statement = null;
    private static ConnexionBdd instance;
    private ConnexionBdd(String dBPath) {
        DBPath = dBPath;
        
    }
    
    public static ConnexionBdd  getInstance(){
    	if(instance==null){
    		instance = new ConnexionBdd("./lib/BDEprojectVGAPPY");
        	instance.connect();
    	}
		return instance;
    }
    public void connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + DBPath);
            statement = connection.createStatement();
            System.out.println("Connexion a " + DBPath + " avec succ�s");
        } catch (ClassNotFoundException notFoundException) {
            notFoundException.printStackTrace();
            System.out.println("Erreur de connexion");
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            System.out.println("Erreur de connexion");
        }
    }
 
    public void close() {
        try {
        	//instance.close();
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public ResultSet query(String requet) {
        ResultSet resultat = null;
        try {
			resultat = statement.executeQuery(requet);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return resultat;
  
    }
    public void update(String query) {
        try {
            statement.executeUpdate(query);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
 
    }
    //TODO: implementer exceptions
}

