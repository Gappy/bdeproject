package com.main.bdeproject.bean;

public class Evenement {
	
	private static int compteur=1;
	private int idEvenement;
	private String type;
	private String libelle;
	private int duree;
	private String date;
	
	public Evenement(){
		//this.idEvenement=compteur;
		compteur++;
	}
    public Evenement(int id,String type,int duree,String date, String libelle){
        this();
        this.idEvenement = id;
        this.type=type;
        this.duree=duree;
        this.date=date;
        this.libelle=libelle;
    }
	public Evenement(String type,int duree,String date, String libelle){
		this(1,type,duree,date,libelle);
	}
	public int getIdEvenement() {
		return idEvenement;
	}
	public void setIdEvenement(int idEvenement) {
		this.idEvenement = idEvenement;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getDuree() {
		return duree;
	}
	public void setDuree(int duree) {
		this.duree = duree;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public String toString(){
		return " "+idEvenement+" "+type+" "+libelle+" "+duree+" "+date;
	}


}
