package com.main.bdeproject.bean;

public class Utilisateur {

	private static int	compteur	= 1;
	private int			idUtilisateur;
	private String		loginUtilisateur;
	private String		password;
	
	
	public Utilisateur(){
		//idUtilisateur=compteur;
		compteur++;
	}
	
	public Utilisateur(int id, String nom,String pass){
		this();
        this.idUtilisateur = id;
		this.loginUtilisateur=nom;
		this.password=pass;
	}

    public Utilisateur(String nom,String pass){
        this(1,nom,pass);
    }


	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}


	public String getLoginUtilisateur() {
		return loginUtilisateur;
	}

	public void setLoginUtilisateur(String nomUtilisateur) {
		this.loginUtilisateur = nomUtilisateur;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public String toString(){
		return idUtilisateur+" "+loginUtilisateur+" "+password;
	}



	
}
