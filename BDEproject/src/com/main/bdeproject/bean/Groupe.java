package com.main.bdeproject.bean;
import java.util.List;
public class Groupe
{
	private static int	compteur	= 1;
	private int			idGroupe;
	private String		nomGroupe;
	private List<Eleve>	listEleve;
	public Groupe()
	{
		this.idGroupe = compteur;
		compteur++;
	}
	public Groupe(String nom)
	{
		this();
		this.nomGroupe = nom;
	}

	public Groupe(String nom, List<Eleve> liste)
	{
		this();
		this.nomGroupe = nom;
		this.listEleve = liste;
	}

	public int getIdGroupe()
	{
		return idGroupe;
	}
	public void setIdGroupe(int idGroupe)
	{
		this.idGroupe = idGroupe;
	}
	public String getNomGroupe()
	{
		return nomGroupe;
	}
	public void setNomGroupe(String nomGroupe)
	{
		this.nomGroupe = nomGroupe;
	}
	public List<Eleve> getListEleve()
	{
		return listEleve;
	}
	public void setListEleve(List<Eleve> listEleve)
	{
		this.listEleve = listEleve;
	}
	
	public String toString(){
		return " "+idGroupe+" "+nomGroupe;
	}
	
	
	

}
