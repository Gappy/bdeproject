package com.main.bdeproject;

import java.sql.DatabaseMetaData;
import com.main.bdeproject.dao.ConnexionBdd;
import com.main.bdeproject.service.*;
import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

public class Principale {
    public static void main (String [] arg){

        Server obj;

        final Router router;
        final Component component;

        try {
          /*  obj = new Server(Protocol.HTTP, 9001, Controller.class);
            obj.start();*/

        	ConnexionBdd connexionBdd = ConnexionBdd.getInstance();
        	
        	
            // Create a new Component.
            component = new Component();

            // Add a new HTTP server listening on port 8182.
            component.getServers().add(Protocol.HTTP, 9001);

            router = new Router(component.getContext().createChildContext());

            router.attach("/helloworld", ServiceHelloWorld.class);
            router.attach("/helloworld/{id}", ServiceHelloWorld.class);

            router.attach("/eleve", ServiceEleve.class);
            router.attach("/eleve/{id}", ServiceEleve.class);

            // Done
            router.attach("/evenement", ServiceEvenement.class);
            router.attach("/evenement/{id}", ServiceEvenement.class);

            router.attach("/groupe", ServiceGroupe.class);
            router.attach("/groupe/{id}", ServiceGroupe.class);

            router.attach("/utilisateur", ServiceUtilisateur.class);
            router.attach("/utilistateur/{id}", ServiceUtilisateur.class);

            // Attach the sample application.
            component.getDefaultHost().attach("/bdeproject", router);

            // Start the component.
            component.start();
        }
        catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
